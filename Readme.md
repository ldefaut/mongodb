# Projet Zoo NodeJS / Mongo DB
Le projet est réalisé avec Node JS, Express et MongoDB. Il met en place une api pour un zoo, avec des animaux et des users.
La base MongoDB est sur Atlas

## Fonctionnalités

- [x] Lister les animaux
- [x] Ajouter un animal
- [ ] Nourrir un animal
- [ ] Faire naitre un animal
- [ ] Adopter un animal
- [x] Se connecter au site
- [x] Créer un utilisateur
- [x] Choisir un zoo préféré
- [x] Générer les fichiers json aléatoirement

### Users

Les utilisateurs peuvent se connecter à l'api via le lien `/login`.

Pour créer un user, il faut passer par la route `/api/user/create` avec l'objet suivant
```
{
    firstname: body.firstname,
    lastname: body.lastname,
    username: body.username,
    password: body.password,
    age: body.age,
    gender: body.gender,
    address: body.address,
}
```
Un zoo favori va être généré suivant l'adresse indiquée par le user.

Ce zoo peut être changé avec la route `/api/user/:id/set-favorite-zoo` en indiquant : 
```
{
    address: body.address,
}
```

### Animaux

Les animaux sont affichés avec la route `/api/animals/`. On peut y passer des paramêtres en get pour filtrer la recherche.

L'objet correspond au schema suivant : 
```
{
    Nom: String,
    Age: Number,
    Type: String,
    Classe: String,
}
```

### Données randoms

Les données peuvent être générées aléatoirement via les routes `/api/random`. La limite initiale est 100, mais on peut la modifier en indiquant un paramêtre get `limit`

La route renvoie un tableau en json avec tous les objets.