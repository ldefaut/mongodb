import { generateUser } from "../services/random.service.js";
import dogNames from "dog-names";
import DB_CLASSIFICATION from "../Datasets/DB_CLASSIFICATIONS.js";

export const randUsers = async (req, res) => {
    let limit = req.query.limit ? req.query.limit : 100;
    let users = [];
    let rand_users = await generateUser(limit);
    for (let i = 0; i < rand_users.results.length; i++) {
        let rand_user = rand_users.results[i];
        let address =
            rand_user.location.street.number +
            " " +
            rand_user.location.street.name +
            ", " +
            rand_user.location.city +
            " " +
            rand_user.location.country;
        let user = {
            firstname: rand_user.name.first,
            lastname: rand_user.name.last,
            username: rand_user.login.username,
            password: rand_user.login.password,
            age: rand_user.dob.age,
            gender: rand_user.name.gender,
            address: address,
        };
        users.push(user);
    }
    res.send(users);
};

export const randAnimals = async (req, res) => {
    let limit = req.query.limit ? req.query.limit : 100;
    let animals = [];
    for (let i = 0; i < limit; i++) {
        let classification =
            DB_CLASSIFICATION[
                Math.floor(
                    Math.random() * (DB_CLASSIFICATION.length - 1 + 1) + 1
                )
            ];
        console.log(classification);
        let animal = {
            Nom: dogNames.allRandom(),
            Age: Math.floor(Math.random() * (100 - 1 + 1) + 1),
            ...classification,
        };
        console.log(animal);
        animals.push(animal);
    }
    res.send(animals);
};
