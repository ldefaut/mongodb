import {
    createAnimal,
    deleteAnimalById,
    getOneById,
    listAnimals,
    updateAnimal,
} from "../services/animal.service.js";

// CRUD Animaux
export const animalDetail = async (req, res) => {
    try {
        const animal = await getOneById(req.params.id);
        res.send(animal);
    } catch {
        res.send("Animal not found");
    }
};

export const animalsList = async (req, res) => {
    const query = req.query;
    const animals = await listAnimals(query);
    res.send(animals);
};

export const animalCreate = async (req, res) => {
    const animal = createAnimal({
        Nom: req.body.Nom,
        Age: req.body.Age,
        Type: req.body.Type,
        Classe: req.body.Classe,
    });
    res.send(animal);
};

export const animalUpdate = async (req, res) => {
    const animal_obj = {
        Nom: req.body.Nom,
        Age: req.body.Age,
        Type: req.body.Type,
        Classe: req.body.Classe,
    };
    const animal = await updateAnimal(req.params.id, animal_obj);
    res.send(animal);
};

export const animalDelete = async (req, res) => {
    const animal = await deleteAnimalById(req.params.id);
    res.send(animal);
};
