import jwt from "jsonwebtoken";
import { findUser } from "../services/user.service.js";

export const login = async (req, res) => {
    const secret = process.env.SECRET;
    const { username, password } = req.body;
    const user = await findUser(username, password);

    if (user) {
        const accessToken = jwt.sign(
            {
                username: user.username,
                password: user.password,
            },
            secret
        );

        res.json(accessToken);
    } else {
        res.send("Username or password is incorrect");
    }
};
