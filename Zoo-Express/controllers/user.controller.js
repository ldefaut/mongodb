import { getClosestPlace, getLatLong } from "../services/location.service.js";
import {
    findUser,
    findUsers,
    createUser,
    updateUser,
} from "../services/user.service.js";
import utf8 from "utf8";

export const getUsers = async (req, res) => {
    const query = req.query;
    const users = await findUsers(query);
    res.send(users);
};

export const userCreate = async (req, res) => {
    const body = req.body;
    let user = await findUsers({ username: body.username });
    if (user.length !== 0) {
        res.send("User already exists");
    } else {
        user = {
            firstname: body.firstname,
            lastname: body.lastname,
            username: body.username,
            password: body.password,
            age: body.age,
            gender: body.gender,
            address: body.address,
            pets: [],
            favorite_zoo: "",
        };
        if (req.body.address) {
            let address = encodeURI(req.body.address);
            let location = await getLatLong(address);
            let closest_zoo = await getClosestPlace(
                "zoo",
                location.results[0].geometry.location.lat,
                location.results[0].geometry.location.lng,
                20000
            );
            user.favorite_zoo = closest_zoo.results[0].vicinity;
        }
        await createUser(user);
        res.send(user);
    }
};

export const changeFavoriteZoo = async (req, res) => {
    let favorite_zoo;
    if (req.body.address) {
        let address = encodeURI(req.body.address);
        let location = await getLatLong(address);
        let closest_zoo = await getClosestPlace(
            "zoo",
            location.results[0].geometry.location.lat,
            location.results[0].geometry.location.lng,
            20000
        );
        favorite_zoo = closest_zoo.results[0].vicinity;
    }

    res.send(await updateUser(req.params.id, { favorite_zoo: favorite_zoo }));
};
