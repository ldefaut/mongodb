import Animal from "../models/animal.model.js";

// Appel de la liste des animaux suivant des paramêtres de recherche (query)
export const listAnimals = (query) => {
    try {
        const animals = Animal.find(query);
        return animals;
    } catch (err) {
        console.log(err);
    }
};

// Recherche dans la bdd un animal via l'id
export const getOneById = (id) => {
    try {
        const animal = Animal.findById(id);
        return animal;
    } catch (err) {
        console.log("Animal not found");
    }
};

// Créer un animal
export const createAnimal = (body) => {
    try {
        const animal = new Animal(body).save();
        return animal;
    } catch (err) {
        console.log(err);
    }
};

export const updateAnimal = (id, body) => {
    try {
        const animal = Animal.updateOne(
            { _id: id },
            {
                $set: body,
            }
        );
        return animal;
    } catch (err) {
        console.log(err);
    }
};

export const deleteAnimalById = (id) => {
    try {
        const delete_res = Animal.findByIdAndDelete(id);
        return delete_res;
    } catch (err) {
        console.log(err);
    }
};
