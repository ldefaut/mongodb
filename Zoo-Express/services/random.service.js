import axios from "axios";

export const generateUser = async (limit) => {
    try {
        const { data: response } = await axios.get(
            "https://randomuser.me/api/?results=" + limit
        ); //use data destructuring to get data from the promise object
        return response;
    } catch (error) {
        console.log(error);
    }
};
