import axios from "axios";

export const getLatLong = async (address) => {
    try {
        const { data: response } = await axios.get(
            "https://maps.googleapis.com/maps/api/geocode/json?address=" +
                address +
                "&key=" +
                process.env.GOOGLE_API_KEY
        ); //use data destructuring to get data from the promise object
        return response;
    } catch (error) {
        console.log(error);
    }
};

export const getClosestPlace = async (type, lat, long, radius) => {
    try {
        const { data: response } = await axios.get(
            "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
                lat +
                "%2C" +
                long +
                "&radius=" +
                radius +
                "&type=" +
                type +
                "&keyword=cruise&key=" +
                process.env.GOOGLE_API_KEY
        ); //use data destructuring to get data from the promise object
        return response;
    } catch (error) {
        console.log(error);
    }
};
