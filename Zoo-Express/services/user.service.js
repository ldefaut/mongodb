import User from "../models/user.model.js";

export const findUsers = (query) => {
    try {
        const users = User.find(query);
        return users;
    } catch {
        console.log(err);
    }
};

export const findUser = (username, password) => {
    try {
        const user = User.findOne({ username: username, password: password });
        return user;
    } catch (err) {
        console.error("User not found");
    }
};

export const createUser = (body) => {
    try {
        const user = User.create(body);
        return user;
    } catch (err) {
        console.log(err);
    }
};

export const updateUser = (id, body) => {
    try {
        const user = User.updateOne(
            { _id: id },
            {
                $set: body,
            }
        );
        return user;
    } catch (err) {
        console.log(err);
    }
};
