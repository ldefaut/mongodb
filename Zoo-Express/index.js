import express from "express";
import mongoose from "mongoose";
import AnimalRouter from "./routes/animal.routes.js";
import userRouter from "./routes/user.routes.js";
import AuthRouter from "./routes/auth.routes.js";
import randomRouter from "./routes/random.routes.js";
import "dotenv/config";
const app = express();

app.use(express.json());

const port = process.env.PORT;
const mongodbUrl = process.env.DATABASE_URL;

mongoose.connect(mongodbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const database = mongoose.connection;

database.on("open", () => {
    console.log("Connection success");

    app.get("/", (req, res) => {
        res.send("Hello world");
    });

    app.listen(port, () => {
        console.log("Server is running on " + port);
    });

    app.use("/api", AnimalRouter);
    app.use("/api", userRouter);
    app.use("/api", randomRouter);
    app.use("/", AuthRouter);
});

database.on("error", () => {
    console.log("Connection failed");
});
