import mongoose from "mongoose";

const schema = mongoose.Schema({
    Nom: String,
    Age: Number,
    Type: String,
    Classe: String,
});

export default mongoose.model("Animal", schema);
