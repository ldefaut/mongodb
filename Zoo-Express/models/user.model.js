import mongoose from "mongoose";

const schema = mongoose.Schema({
    firstname: String,
    lastname: String,
    username: String,
    password: String,
    age: Number,
    gender: String,
    address: String,
    pets: Array,
    favorite_zoo: String,
});

export default mongoose.model("User", schema);
