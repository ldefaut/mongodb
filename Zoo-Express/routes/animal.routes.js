import express from "express";
import * as AnimalController from "../controllers/animals.controller.js";
import { authenticateJWT } from "../middleware/authentification.js";

const router = express.Router();

// Listing des animaux
router.get("/animals", AnimalController.animalsList);

// Détail d'un animal
router.get("/animal/:id", authenticateJWT, AnimalController.animalDetail);

// Création d'un animal
router.post("/animal/create", authenticateJWT, AnimalController.animalCreate);

// Modification d'un animal
router.post(
    "/animal/:id/update/",
    authenticateJWT,
    AnimalController.animalUpdate
);

// Suppression d'un animal
router.post(
    "/animal/:id/delete/",
    authenticateJWT,
    AnimalController.animalDelete
);

export default router;
