import express from "express";
import * as randomController from "../controllers/random.controller.js";

const router = express.Router();

router.get("/random/users", randomController.randUsers);

router.get("/random/animals", randomController.randAnimals);
export default router;
