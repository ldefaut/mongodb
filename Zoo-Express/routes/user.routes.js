import express from "express";
import * as userController from "../controllers/user.controller.js";
import { authenticateJWT } from "../middleware/authentification.js";

const router = express.Router();

router.get("/users", authenticateJWT, userController.getUsers);

router.post("/user/create", userController.userCreate);

router.post(
    "/user/:id/set-favorite-zoo",
    authenticateJWT,
    userController.changeFavoriteZoo
);

export default router;
