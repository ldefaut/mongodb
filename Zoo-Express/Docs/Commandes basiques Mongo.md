## Find
- `db.<db_name>.find`--> Chercher dans la bdd
- `db.<db_name>.find( { key : { <operator> : "value" } } )` --> *Chercher dans la bdd (Avec Operator)*
- `db.<db_name>.find( { key1: "value1" , key2: "value2" } )` --> *Chercher dans la bdd (AND)*
- `db.<db_name>.find( { $or: [ { key1: "value1" }, { key2: "value2" } ] } )` --> *Chercher dans la bdd (OR)*
- `db.<db_name>.find( {}, { key1 : 1, key2 : 1, key3 : 1 })` --> *Affiche les parametres choisis (objet2) sur tous les objets (objet1)*

## Insert
- `db.<db_name>.insertOne( { key: "value", ... } )` --> *Insert dans la bdd un doc*
- `db.<db_name>.insertMany([ { key: "value", ... } , { key: "value", ... } ])` --> *Insert dans la bdd plusieurs docs*

## Use
- `use <db_name>` --> *utilise telle db*

## Debug
- `mongoose.set('debug', true);` --> *Affiche les parametres de la recherche mongo*