# Projet Zoo

## Description
Site de gestion d'un zoo. Le zoo possède des animaux qui peuvent naitre, être nourris, être adoptés ou mourir (:cry:).

## Fonctionnalités
- [x] Lister les animaux
- [x] Ajouter un animal
- [ ] Nourrir un animal
- [ ] Faire naitre un animal
- [ ] Adopter un animal
- [x] Se connecter au site
- [x] Créer un utilisateur
- [x] Choisir un zoo préféré
- [x] Générer les fichiers json aléatoirement
