export default [
    {
        Type: "aardvark",
        Classe: "Mamifère",
    },
    {
        Type: "abeille",
        Classe: "Insecte",
    },
    {
        Type: "alouette",
        Classe: "Oiseau",
    },
    {
        Type: "antilope",
        Classe: "Mamifère",
    },
    {
        Type: "autruche",
        Classe: "Oiseau",
    },
    {
        Type: "basse",
        Classe: "Poisson",
    },
    {
        Type: "brochet",
        Classe: "Poisson",
    },
    {
        Type: "buffle",
        Classe: "Mamifère",
    },
    {
        Type: "campagnol",
        Classe: "Mamifère",
    },
    {
        Type: "canard",
        Classe: "Oiseau",
    },
    {
        Type: "carpe",
        Classe: "Poisson",
    },
    {
        Type: "cerf",
        Classe: "Mamifère",
    },
    {
        Type: "chauve-souris",
        Classe: "Mamifère",
    },
    {
        Type: "chevaine",
        Classe: "Poisson",
    },
    {
        Type: "chèvre",
        Classe: "Mamifère",
    },
    {
        Type: "chien de mer",
        Classe: "Poisson",
    },
    {
        Type: "cobaye",
        Classe: "Mamifère",
    },
    {
        Type: "coccinelle",
        Classe: "Insecte",
    },
    {
        Type: "colombe",
        Classe: "Oiseau",
    },
    {
        Type: "corbeau",
        Classe: "Oiseau",
    },
    {
        Type: "crabe",
        Classe: "Invertébré",
    },
    {
        Type: "crapaud",
        Classe: "Amphibien",
    },
    {
        Type: "cygne",
        Classe: "Oiseau",
    },
    {
        Type: "dauphin",
        Classe: "Mamifère",
    },
    {
        Type: "écrevisse",
        Classe: "Invertébré",
    },
    {
        Type: "écumoire",
        Classe: "Oiseau",
    },
    {
        Type: "écureuil",
        Classe: "Mamifère",
    },
    {
        Type: "églefin",
        Classe: "Poisson",
    },
    {
        Type: "étoile de mer",
        Classe: "Invertébré",
    },
    {
        Type: "faisan",
        Classe: "Oiseau",
    },
    {
        Type: "faucon",
        Classe: "Oiseau",
    },
    {
        Type: "fille",
        Classe: "Mamifère",
    },
    {
        Type: "flamant",
        Classe: "Oiseau",
    },
    {
        Type: "girafe",
        Classe: "Mamifère",
    },
    {
        Type: "gorille",
        Classe: "Mamifère",
    },
    {
        Type: "grenouille",
        Classe: "Amphibien",
    },
    {
        Type: "grenouille",
        Classe: "Amphibien",
    },
    {
        Type: "guépard",
        Classe: "Mamifère",
    },
    {
        Type: "guêpe",
        Classe: "Insecte",
    },
    {
        Type: "guêpe",
        Classe: "Invertébré",
    },
    {
        Type: "hamster",
        Classe: "Mamifère",
    },
    {
        Type: "hareng",
        Classe: "Poisson",
    },
    {
        Type: "hippocampe",
        Classe: "Poisson",
    },
    {
        Type: "homard",
        Classe: "Invertébré",
    },
    {
        Type: "kiwi",
        Classe: "Oiseau",
    },
    {
        Type: "l'éléphant",
        Classe: "Mamifère",
    },
    {
        Type: "labbe",
        Classe: "Oiseau",
    },
    {
        Type: "léopard",
        Classe: "Mamifère",
    },
    {
        Type: "lièvre",
        Classe: "Mamifère",
    },
    {
        Type: "limace",
        Classe: "Invertébré",
    },
    {
        Type: "lion",
        Classe: "Mamifère",
    },
    {
        Type: "lion de mer",
        Classe: "Mamifère",
    },
    {
        Type: "loup",
        Classe: "Mamifère",
    },
    {
        Type: "lynx",
        Classe: "Mamifère",
    },
    {
        Type: "manchot",
        Classe: "Oiseau",
    },
    {
        Type: "mangouste",
        Classe: "Mamifère",
    },
    {
        Type: "marsouin",
        Classe: "Mamifère",
    },
    {
        Type: "minou",
        Classe: "Mamifère",
    },
    {
        Type: "moineau",
        Classe: "Oiseau",
    },
    {
        Type: "môle",
        Classe: "Mamifère",
    },
    {
        Type: "mouche domestique",
        Classe: "Insecte",
    },
    {
        Type: "moucheron",
        Classe: "Insecte",
    },
    {
        Type: "mouette",
        Classe: "Oiseau",
    },
    {
        Type: "nandou",
        Classe: "Oiseau",
    },
    {
        Type: "opossum",
        Classe: "Mamifère",
    },
    {
        Type: "ornithorynque",
        Classe: "Mamifère",
    },
    {
        Type: "oryx",
        Classe: "Mamifère",
    },
    {
        Type: "ours",
        Classe: "Mamifère",
    },
    {
        Type: "palourde",
        Classe: "Invertébré",
    },
    {
        Type: "papillon",
        Classe: "Insecte",
    },
    {
        Type: "perruche",
        Classe: "Oiseau",
    },
    {
        Type: "piranha",
        Classe: "Poisson",
    },
    {
        Type: "poisson-chat",
        Classe: "Poisson",
    },
    {
        Type: "poney",
        Classe: "Mamifère",
    },
    {
        Type: "poulet",
        Classe: "Oiseau",
    },
    {
        Type: "poulpe",
        Classe: "Invertébré",
    },
    {
        Type: "puce",
        Classe: "Insecte",
    },
    {
        Type: "puma",
        Classe: "Mamifère",
    },
    {
        Type: "putois",
        Classe: "Mamifère",
    },
    {
        Type: "raie",
        Classe: "Poisson",
    },
    {
        Type: "raton laveur",
        Classe: "Mamifère",
    },
    {
        Type: "renne",
        Classe: "Mamifère",
    },
    {
        Type: "roitelet",
        Classe: "Oiseau",
    },
    {
        Type: "sanglier",
        Classe: "Mamifère",
    },
    {
        Type: "sceller",
        Classe: "Mamifère",
    },
    {
        Type: "scorpion",
        Classe: "Invertébré",
    },
    {
        Type: "serpent de mer",
        Classe: "Reptile",
    },
    {
        Type: "termite",
        Classe: "Insecte",
    },
    {
        Type: "thon",
        Classe: "Poisson",
    },
    {
        Type: "tortue",
        Classe: "Reptile",
    },
    {
        Type: "triton",
        Classe: "Amphibien",
    },
    {
        Type: "tuatara",
        Classe: "Reptile",
    },
    {
        Type: "unique",
        Classe: "Poisson",
    },
    {
        Type: "vampire",
        Classe: "Mamifère",
    },
    {
        Type: "vautour",
        Classe: "Oiseau",
    },
    {
        Type: "veau",
        Classe: "Mamifère",
    },
    {
        Type: "ver de terre",
        Classe: "Invertébré",
    },
    {
        Type: "ver lent",
        Classe: "Reptile",
    },
    {
        Type: "vipère",
        Classe: "Reptile",
    },
    {
        Type: "vison",
        Classe: "Mamifère",
    },
    {
        Type: "wallaby",
        Classe: "Mamifère",
    },
];
